# Aria2

## Default configuration
* download path: /data
* configuration file: /aria2/aria2.conf
* session: /aria2/aria2.session
* rpc-secret: (empty)

### docker run
```
docker run -v /custom-path:/data -p $PORT:6800 vancez/aria2
```
